const express = require('express');
const router = express.Router();

const jsf = require('json-schema-faker');
const util = require('util')
const chance = require('chance')
const faker = require('faker')
jsf.extend('chance', () => new chance.Chance());
jsf.extend('faker', () => faker);

var recentDays = 5;

var schema = {
  "type": "array",
  "minItems": 20,
  "maxItems": 40,
  "items": {
	  "type": "object",
	  "properties": {
	    "name": {
	      "type": "string",
	      "faker": "name.findName"
	    },
	    "date": {
	      "type": "string",
	      "faker": "date.recent"
	    },
	    "rank" : {
	      "type": "integer", 
	       "minimum": 6,
  		   "maximum": 10
	    },
	    "email": {
            "type": "string",
            "faker": "internet.email"
          },
          "feed": {
            "type": "string",
            "pattern": "Fine|Super, it is very convenient to use|Wonderful|definitely a very good choice| It helps me a lot | I highly recommend it to everyone| Cool | Amaizing | It is very useful| Super| Chips the best"
          }
	  },
	  "required": [
	    "name",
	    "date",
	    "rank",
	    "email",
	    "feed"
	   ]
	  }
};

/* GET home page. */
router.get('/', (req, res) => {

  jsf.resolve(schema).then(sample => {
  	   console.log(util.inspect(sample, 
  	   	{showHidden: false, depth: null}));
	   
	   res.render('feedback', 
	  	{  opinions:  sample });
  });

  
});

module.exports = router;
