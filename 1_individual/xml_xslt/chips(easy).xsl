<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <head>
                <style>
                    body{margin-top:15px}
                    h1,h3{padding-left:30px}
                    img{width:70px;height:70px}
                </style>
            </head>
            <body>
                <h1>All models we provide</h1>
                <ul>
                    <xsl:apply-templates select="//chip"/>
                </ul>
                <h2>Number of chip models available:<xsl:value-of select="count(//chip)"/></h2>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="chip">
        <div>
            <br/>Model : <xsl:value-of select="@model"/>
            <br/> date :  <xsl:value-of select="@dateRelise"/>
            <br/>price : <xsl:value-of select="@price"/>
            <br/>category : <xsl:value-of select="@category"/>
            <br/>powerful : <xsl:value-of select="@powerful"/></div>
        <hr/>
    </xsl:template>

</xsl:stylesheet>