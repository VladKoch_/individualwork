<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:key name="chips-by-powerful" match="chip" use="@powerful"/>
    <xsl:template match="/">
        <html>
            <body>
                <h1>
                    <xsl:value-of select="/images/@title"/>
                </h1>
                <xsl:apply-templates select="//chip[generate-id(.) = generate-id(key('chips-by-powerful', @powerful)[1])]">
                    <xsl:sort select="@powerful" order="ascending"/>
                </xsl:apply-templates>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="chip">
        <h3>
            Powerful:
            <xsl:value-of select="@powerful"/>
        </h3>
        <p>
            Total chips in this powerful:
            <xsl:value-of select="count(//chip[@powerful = current()/@powerful])"/>
        </p>
        <ul>
            <xsl:apply-templates select="key('chips-by-powerful', @powerful)" mode="grouping">
                <xsl:sort select="@price" order="ascending"/>
            </xsl:apply-templates>
        </ul>
    </xsl:template>
    <xsl:template match="chip" mode="grouping">
        <li>
            <h4>
                Model:
                <xsl:value-of select="@model"/>
            </h4>
            <p>
                Price:
                <xsl:value-of select="@price"/>
            </p>
            <p>
                Type:
                <xsl:value-of select="@category"/>
            </p>
        </li>
    </xsl:template>
</xsl:stylesheet>