/**
 * FP All models
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 0.0.5
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/* tslint:disable:no-unused-variable member-ordering */

import { Inject, Injectable, Optional }                      from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams,
         HttpResponse, HttpEvent }                           from '@angular/common/http';
import { CustomHttpUrlEncodingCodec }                        from '../encoder';

import { Observable }                                        from 'rxjs/Observable';

import { ChipsType } from '../model/chipsType';
import { Chipslist } from '../model/chipslist';
import { ErrorResponse } from '../model/errorResponse';
import { ModelList } from '../model/modelList';

import { BASE_PATH, COLLECTION_FORMATS }                     from '../variables';
import { Configuration }                                     from '../configuration';


@Injectable()
export class DefaultService {

    protected basePath = 'http://localhost:10011/api/v1';
    public defaultHeaders = new HttpHeaders();
    public configuration = new Configuration();

    constructor(protected httpClient: HttpClient, @Optional()@Inject(BASE_PATH) basePath: string, @Optional() configuration: Configuration) {
        if (basePath) {
            this.basePath = basePath;
        }
        if (configuration) {
            this.configuration = configuration;
            this.basePath = basePath || configuration.basePath || this.basePath;
        }
    }

    /**
     * @param consumes string[] mime-types
     * @return true: consumes contains 'multipart/form-data', false: otherwise
     */
    private canConsumeForm(consumes: string[]): boolean {
        const form = 'multipart/form-data';
        for (const consume of consumes) {
            if (form === consume) {
                return true;
            }
        }
        return false;
    }


    /**
     * 
     * List of chips types to choose
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public chipTypes(observe?: 'body', reportProgress?: boolean): Observable<Array<ChipsType>>;
    public chipTypes(observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Array<ChipsType>>>;
    public chipTypes(observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Array<ChipsType>>>;
    public chipTypes(observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json'
        ];

        return this.httpClient.get<Array<ChipsType>>(`${this.basePath}/chips/types`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * 
     * List of chips in stok to purchases
     * @param modelId Chip&#39;s model to filter out
     * @param chipsType Type of the chips
     * @param chipsName Name of this model of chip
     * @param page Page to iterate, 1 is default
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public chipsList(modelId?: string, chipsType?: string, chipsName?: string, page?: number, observe?: 'body', reportProgress?: boolean): Observable<Array<Chipslist>>;
    public chipsList(modelId?: string, chipsType?: string, chipsName?: string, page?: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Array<Chipslist>>>;
    public chipsList(modelId?: string, chipsType?: string, chipsName?: string, page?: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Array<Chipslist>>>;
    public chipsList(modelId?: string, chipsType?: string, chipsName?: string, page?: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {





        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (modelId !== undefined && modelId !== null) {
            queryParameters = queryParameters.set('modelId', <any>modelId);
        }
        if (chipsType !== undefined && chipsType !== null) {
            queryParameters = queryParameters.set('ChipsType', <any>chipsType);
        }
        if (chipsName !== undefined && chipsName !== null) {
            queryParameters = queryParameters.set('chipsName', <any>chipsName);
        }
        if (page !== undefined && page !== null) {
            queryParameters = queryParameters.set('page', <any>page);
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json'
        ];

        return this.httpClient.get<Array<Chipslist>>(`${this.basePath}/chips/list`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * 
     * List of spots,  where you can find our chips
     * @param model The model for filtering
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public listModels(model?: string, observe?: 'body', reportProgress?: boolean): Observable<Array<ModelList>>;
    public listModels(model?: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Array<ModelList>>>;
    public listModels(model?: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Array<ModelList>>>;
    public listModels(model?: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {


        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (model !== undefined && model !== null) {
            queryParameters = queryParameters.set('model', <any>model);
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json'
        ];

        return this.httpClient.get<Array<ModelList>>(`${this.basePath}/chips/models`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

}
