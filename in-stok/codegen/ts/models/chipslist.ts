/**
 * FP All models
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 0.0.5
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


export interface Chipslist { 
    id: string;
    chipType?: string;
    chipName?: string;
    releaseDate?: string;
    price?: number;
}
