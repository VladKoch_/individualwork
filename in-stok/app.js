'use strict';

var appRoot = require('app-root-path');//добавил
var reqlib = appRoot.require;//добавил

var SwaggerExpress = require('swagger-express-mw');
const express = require('express');
const swStats = require('swagger-stats'); // добавил
const apiSpec = reqlib('api/swagger/openapi.json') // добавил

const path = require('path');
var app = express();

module.exports = app; // for testing

var config = {
  appRoot: __dirname // required config
};


// to expose health check 
app.use('/health', require('express-healthcheck')());

// to serve docs
app.use(express.static(path.join(__dirname, 'public')));


SwaggerExpress.create(config, function(err, swaggerExpress) {
  if (err) { throw err; }

// allow swagger-stats microservice monitoring by http://localhost:8081/swagger-stats/ux#/
app.use(swStats.getMiddleware({swaggerSpec:apiSpec})); // добавил
  // install middleware
  swaggerExpress.register(app);



 /*var port = process.env.PORT || 10011;*/
 var port = 8081;
  app.listen(port);

  console.log('check this to see docs :\n  http://127.0.0.1:' + port + '/');
  
  if (swaggerExpress.runner.swagger.paths['/chips/list']) {
  	console.log('try this to test:\ncurl http://127.0.0.1:' + port + '/api/v1/chips/list');

  }
});
